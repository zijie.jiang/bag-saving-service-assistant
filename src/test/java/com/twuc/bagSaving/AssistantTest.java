package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AssistantTest extends BagSavingArgument{

    @Test
    void should_return_ticket_when_save_a_small_bag_given_cabinet_with_full_small_lockers() {
        Bag smallBag = new Bag(BagSize.SMALL);
        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.SMALL, 10));
        Assistant assistant = new Assistant();
        Ticket ticket = assistant.save(smallBag, cabinet, LockerSize.SMALL);
        assertNotNull(ticket);
    }

    @Test
    void should_return_error_message_when_save_a_small_bag_given_cabinet_with_empty_small_lockers() {
        Bag smallBag = new Bag(BagSize.SMALL);
        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.BIG, 10));
        Assistant assistant = new Assistant();

        InsufficientLockersException exception =
                assertThrows(InsufficientLockersException.class,
                        () -> assistant.save(smallBag, cabinet, LockerSize.SMALL));
        assertEquals("Insufficient empty lockers.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createCorrespondingSizeBagAndLockerSize"})
    void should_return_ticket_when_save_bag_given_cabinet_with_full_lockers(BagSize bagSize, LockerSize lockerSize) {
        Bag bag = new Bag(bagSize);
        Cabinet cabinet = new Cabinet(new LockerSetting(lockerSize, 10));
        Assistant assistant = new Assistant();

        Ticket ticket = assistant.save(bag, cabinet, lockerSize);
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag_when_get_bag_by_assistant_given_a_ticket() {
        Bag smallBag = new Bag(BagSize.SMALL);
        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.SMALL, 10));
        Assistant assistant = new Assistant();
        Ticket ticket = assistant.save(smallBag, cabinet, LockerSize.SMALL);

        Bag bag = cabinet.getBag(ticket);
        assertSame(smallBag, bag);
    }

    @Test
    void should_get_error_message_when_get_bag_by_assistant_given_a_ticket() {
        Bag smallBag = new Bag(BagSize.SMALL);
        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.SMALL, 10));
        Assistant assistant = new Assistant();
        Ticket ticket = assistant.save(smallBag, cabinet, LockerSize.SMALL);

        cabinet.getBag(ticket);

        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> cabinet.getBag(ticket));

        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createCorrespondingSizeBagAndLockerSize"})
    void should_get_ticket_when_save_bag_by_assistant_given_two_cabinet_with_full_lockers(BagSize bagSize, LockerSize lockerSize) {
        Cabinet oneCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Cabinet anotherCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();

        Bag bag = new Bag(bagSize);
        Assistant assistant = new Assistant();
        Ticket ticket = assistant.save(bag, Arrays.asList(oneCabinet, anotherCabinet), lockerSize);

        Bag bagFromOneCabinet = oneCabinet.getBag(ticket);
        assertSame(bag, bagFromOneCabinet);
    }

    @Test
    void should_get_ticket_when_save_bag_by_assistant_given_one_cabinet_with_empty_lockers_and_another_cabinet_with_full_lockers() {
        Cabinet emptyLockersCabinet = CabinetFactory.createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 10);
        Cabinet fullLockersCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();

        Bag bag = new Bag(BagSize.SMALL);
        List<Cabinet> cabinets = Arrays.asList(emptyLockersCabinet, fullLockersCabinet);
        Assistant assistant = new Assistant(cabinets);
        Ticket ticket = assistant.save(bag, cabinets, LockerSize.SMALL);

        Bag bagFromCabinet = assistant.getBag(ticket);
        assertSame(bag, bagFromCabinet);
    }

    @Test
    void should_return_error_message_when_save_by_assistant_given_two_cabinet_with_empty_lockers() {
        Cabinet emptyLockersOneCabinet = CabinetFactory.createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 10);
        Cabinet emptyLockersAnotherCabinet = CabinetFactory.createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 10);

        Bag bag = new Bag(BagSize.SMALL);
        List<Cabinet> cabinets = Arrays.asList(emptyLockersOneCabinet, emptyLockersAnotherCabinet);
        Assistant assistant = new Assistant(cabinets);

        InsufficientLockersException exception =
                assertThrows(InsufficientLockersException.class,
                        () -> assistant.save(bag, cabinets, LockerSize.SMALL));
        assertEquals("Insufficient empty lockers.", exception.getMessage());
    }

    @Test
    void should_get_bag_when_save_bag_by_assistant1_and_get_bag_by_assistant2_given_two_cabinet_and_a_ticket() {
        Cabinet oneCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Cabinet anotherCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();

        Bag bag = new Bag(BagSize.SMALL);
        List<Cabinet> cabinets = Arrays.asList(oneCabinet, anotherCabinet);
        Assistant oneAssistant = new Assistant(cabinets);
        Assistant anotherAssistant = new Assistant(cabinets);

        Ticket ticket = oneAssistant.save(bag, LockerSize.SMALL);
        Bag savedBag = anotherAssistant.getBag(ticket);
        assertSame(bag, savedBag);
    }

    @Test
    void should_return_error_message_when_save_bag_and_get_by_assistant_given_two_cabinet_a_invalid_ticket() {
        Cabinet oneCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Cabinet anotherCabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();

        Bag bag = new Bag(BagSize.SMALL);
        List<Cabinet> cabinets = Arrays.asList(oneCabinet, anotherCabinet);
        Assistant assistant = new Assistant(cabinets);

        Ticket ticket = assistant.save(bag, LockerSize.SMALL);
        assistant.getBag(ticket);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            assistant.getBag(ticket);
        });

        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_get_ticket_when_save_smaller_bag_by_lazy_assistant_given_two_cabinet_with_full_bigger_lockers(BagSize bagSize,
                                                                                                              LockerSize lockerSize
    ) {
        Cabinet oneBiggerLockerCabinet = new Cabinet(new LockerSetting(lockerSize, 10));
        Cabinet anotherBiggerLockerCabinet = new Cabinet(new LockerSetting(lockerSize, 10));

        Bag smallerBag = new Bag(bagSize);
        List<Cabinet> cabinets = Arrays.asList(oneBiggerLockerCabinet, anotherBiggerLockerCabinet);
        LazyAssistant lazyAssistant = new LazyAssistant(cabinets);
        Ticket ticket = lazyAssistant.save(smallerBag, lockerSize);

        assertNotNull(ticket);
    }
}
