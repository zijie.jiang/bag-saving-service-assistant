package com.twuc.bagSaving;

import java.util.Arrays;

public enum LockerSize {
    BIG(40),
    MEDIUM(30),
    SMALL(20);

    private final int sizeNumber;

    LockerSize(int sizeNumber) {
        this.sizeNumber = sizeNumber;
    }

    int getSizeNumber() {
        return sizeNumber;
    }

    public static LockerSize getMoreSize(LockerSize lockerSize) {
        if (lockerSize.getSizeNumber() + 10 > 40) {
            throw new InsufficientLockersException("Insufficient empty lockers.");
        }
        for (LockerSize locker : LockerSize.values()) {
            if (locker.getSizeNumber() == lockerSize.getSizeNumber() + 10) {
                return locker;
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }
}
