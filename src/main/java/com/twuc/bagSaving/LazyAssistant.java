package com.twuc.bagSaving;

import java.util.List;

public class LazyAssistant extends Assistant {
    public LazyAssistant(List<Cabinet> cabinets) {
        super(cabinets);
    }

    @Override
    public Ticket save(Bag bag, LockerSize lockerSize) {
        for (Cabinet cabinet : managedCabinets) {
            Locker locker = cabinet.getLockerByLockerSize(lockerSize);
            if (locker.getCapacity() > 0) {
                return cabinet.save(bag, lockerSize);
            }
        }
        while (true) {
            lockerSize = LockerSize.getMoreSize(lockerSize);
            for (Cabinet cabinet : managedCabinets) {
                Locker locker = cabinet.getLockerByLockerSize(lockerSize);
                if (locker.getCapacity() > 0) {
                    return cabinet.save(bag, lockerSize);
                }
            }
        }
    }
}
