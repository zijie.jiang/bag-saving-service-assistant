package com.twuc.bagSaving;

import java.util.HashMap;
import java.util.Map;

class Locker {
    private final int capacity;
    private final Map<Ticket, Bag> ticketBagMap = new HashMap<>();

    Locker(int capacity) {
        this.capacity = capacity;
    }

    boolean hasTicket(Ticket ticket) {
        return ticketBagMap.containsKey(ticket);
    }

    Ticket saveBag(Bag bag) {
        if (capacity <= ticketBagMap.size()) {
            throw new InsufficientLockersException("Insufficient empty lockers.");
        }
        Ticket ticket = new Ticket();
        ticketBagMap.put(ticket, bag);
        return ticket;
    }

    Bag getBag(Ticket ticket) {
        if (!ticketBagMap.containsKey(ticket)) {
            return null;
        }

        Bag bag = ticketBagMap.get(ticket);
        ticketBagMap.remove(ticket);
        return bag;
    }

    public int getCapacity() {
        return capacity;
    }
}
