package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Cabinet {
    private final Map<LockerSize, Locker> lockers;

    public Cabinet(LockerSetting... lockerSettings) {
        validate(lockerSettings);
        lockers = createLocker(lockerSettings);
    }

    public Ticket save(Bag bag, LockerSize lockerSize) {
        if (lockerSize == null) {
            throw new IllegalArgumentException("Please specify locker size");
        }

        if (bag == null) {
            throw new IllegalArgumentException("Please at least put something here.");
        }

        validateBagAndLockerSize(bag.getBagSize(), lockerSize);

        if (!lockers.containsKey(lockerSize)) {
            throw new IllegalArgumentException("Not supported locker size: " + lockerSize);
        }

        return lockers.get(lockerSize).saveBag(bag);
    }

    public Bag getBag(Ticket ticket) {
        if (ticket == null) {
            throw new IllegalArgumentException("Please use your ticket.");
        }

        Locker targetLocker = lockers.values().stream()
            .filter(locker -> locker.hasTicket(ticket))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Invalid ticket."));

        return targetLocker.getBag(ticket);
    }

    private Map<LockerSize, Locker> createLocker(LockerSetting[] lockerSettings) {
        Map<LockerSize, Locker> lockerSettingsMap = Arrays.stream(lockerSettings)
            .collect(Collectors.toMap(LockerSetting::getLockerSize,
                ls -> new Locker(ls.getCapacity())));
        for (LockerSize lockerSize : LockerSize.values()) {
            if (!lockerSettingsMap.containsKey(lockerSize)) {
                lockerSettingsMap.put(lockerSize, new Locker(0));
            }
        }

        return lockerSettingsMap;
    }

    private void validate(LockerSetting[] lockerSettings) {
        boolean allLockersEmpty = Arrays.stream(lockerSettings).allMatch(ls -> ls.getCapacity() < 1);
        if (allLockersEmpty) {
            throw new IllegalArgumentException("Invalid capacity: 0");
        }

        Optional<Integer> lessThanZeroCapacity =
            Arrays.stream(lockerSettings).filter(ls -> ls.getCapacity() < 0)
                .map(LockerSetting::getCapacity).findFirst();
        if (lessThanZeroCapacity.isPresent()) {
            throw new IllegalArgumentException(String.format("Invalid capacity: %d", lessThanZeroCapacity.get()));
        }
    }

    private void validateBagAndLockerSize(BagSize bagSize, LockerSize lockerSize) {
        if (bagSize.getSizeNumber() > lockerSize.getSizeNumber()) {
            throw new IllegalArgumentException(
                String.format("Cannot save %s bag to %s locker.", bagSize, lockerSize));
        }
    }

    public Locker getLockerByLockerSize(LockerSize lockerSize) {
        if (!lockers.containsKey(lockerSize)) {
            throw new IllegalArgumentException("Not supported locker size: " + lockerSize);
        }
        return lockers.get(lockerSize);
    }
}
