package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class Assistant {

    protected List<Cabinet> managedCabinets = new ArrayList<>();

    public Assistant() {
    }

    public Assistant(List<Cabinet> managedCabinets) {
        this.managedCabinets = managedCabinets;
    }

    public Ticket save(Bag smallBag, Cabinet cabinet, LockerSize lockerSize) {
        return cabinet.save(smallBag, lockerSize);
    }

    public Ticket save(Bag bag, LockerSize lockerSize) {
        for (Cabinet cabinet : this.managedCabinets) {
            try {
                return cabinet.save(bag, lockerSize);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }

    public Ticket save(Bag bag, List<Cabinet> cabinets, LockerSize lockerSize) {
        for (Cabinet cabinet : cabinets) {
            try {
                return cabinet.save(bag, lockerSize);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }


    public Bag getBag(Ticket ticket) {
        for (Cabinet cabinet : this.managedCabinets) {
            try {
                return cabinet.getBag(ticket);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        throw new IllegalArgumentException("Invalid ticket.");
    }
}
